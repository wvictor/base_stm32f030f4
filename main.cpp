#include "mbed.h"
#include "ssd1306.h"

#include "dejavu_sans_cond_22x32.h"
const unsigned char *font = DejaVu_Sans_Condensed22x32;
const char *font_chars = DejaVu_Sans_Condensed22x32_chars;

DigitalOut myled(PA_4);

I2C i2c(I2C_SDA, I2C_SCL);
SSD1306 oled(&i2c, 0x78);

void draw_number(SSD1306 *display, uint16_t num)
{
    char base = '0';
    char digits[5];

    if (num < 9999) {
        digits[0] = base + ((num / 1000) % 10);
        digits[1] = '.';
        digits[2] = base + ((num / 100) % 10);
        digits[3] = base + ((num / 10) % 10);
        digits[4] = base + (num % 10);
    } else {
        for (int i = 0; i < 5; i++) {
            digits[i] = '-';
        }
        digits[1] = '.';
    }
    for (int i = 0; i < 5; i++) {
        display->draw_char(2, 4 + (24 * i), digits[i]);
    }
}

int main() {

    printf("\nHello World!!\n");

    i2c.frequency(400000);
    printf("- I2C freq set.\n");

    oled.init();
    printf("- OLED init() done.\n");

    wait(2.0);

    oled.test_display();
    printf("- OLED test_display() done.\n");

    oled.clear_display();

    oled.set_font_data(font, 4, 22, font_chars, 12);
    printf("- OLED set_font_data() done.\n");
    fflush(stdout);

    /*
    oled.draw_char(0, 0, '0');
    oled.draw_char(0, 22, '1');
    oled.draw_char(0, 44, '.');
    oled.draw_char(0, 66, '2');
    oled.draw_char(0, 88, '3');
    oled.draw_char(4, 0, '-');
    oled.draw_char(4, 22, '-');
    oled.draw_char(4, 44, '8');
    oled.draw_char(4, 66, '-');
    oled.draw_char(4, 88, '-');
    printf("- OLED draw_char() done.\n");
    fflush(stdout);
*/
 /*
    draw_number(&oled, 5619);
    oled.display();
    printf("- draw_number(5619) done.\n");
    fflush(stdout);
    wait(2.0);

    draw_number(&oled, 15619);
    oled.display();
    printf("- draw_number(15619) done.\n");
    fflush(stdout);
    wait(2.0);
*/
    int n = 0;

    while(1) {
        for(int i=0; i<6; i++) {
            myled = i % 2;
            wait(0.1);
        }
        printf(".");
        fflush(stdout);
        wait(0.4);
        oled.clear_fb();
        draw_number(&oled, n++);
        oled.display();
    }
}
