#include "ssd1306.h"
#include <string.h>

SSD1306::SSD1306(I2C *i__i2c, int i__slave_addr) {
    _i2c = i__i2c;
    _slave_addr = i__slave_addr;
};

void SSD1306::init() {
    init_device();
    clear_display();
}

void SSD1306::display() {
    send_data(_framebuf, SSD1306_FRAMEBUF_SIZE);
}

void SSD1306::test_display() {
    for (int i = 0; i < SSD1306_FRAMEBUF_SIZE; i++) {
        if ((i % 8) == 0 || (i % 8) == 7) {
            _framebuf[i] = 0xff;
        } else {
            _framebuf[i] = 0x81;
        }
    }
    display();
}

void SSD1306::clear_display() {
    clear_fb();
    display();
}

void SSD1306::clear_fb() {
    memset(_framebuf, 0, SSD1306_FRAMEBUF_SIZE);
}

void SSD1306::set_fb_byte(int page, int col, char val) {
    _framebuf[(page * 128) + col] = val;
}

void SSD1306::set_font_data(const unsigned char *data, int byte_rows, int cols, const char *chars, int char_count) {
    _fontdata = data;
    _font_byte_rows = byte_rows;
    _font_columns = cols;
    _font_chars = chars;
    _font_char_count = char_count;
}

bool SSD1306::draw_char(int byte_y, int x, char c) {
    int n = 0;
    const unsigned char *char_fontdata;

    for (n = 0; n < _font_char_count; n++) {
        if (_font_chars[n] == c) {
            break;
        }
    }

    if (n >= _font_char_count) {
        return false;
    }

    char_fontdata = _fontdata + (_font_columns * _font_byte_rows * n);
    for (int page = 0; page < _font_byte_rows; page++) {
        for (int col = 0; col < _font_columns; col++) {
            set_fb_byte(page + byte_y, col + x, char_fontdata[col * _font_byte_rows + page]);
        }
    }
    
    return true;
}

void SSD1306::send_command(char cmd) {
    _i2c->lock();
    _i2c->start();
    _i2c->write(_slave_addr);
    _i2c->write(SSD1306_COMMAND_MODE);
    _i2c->write(cmd);
    _i2c->stop();
    _i2c->unlock();
}
  
void SSD1306::send_data(const char *buf, int len) {
    send_command(SSD1306_PAGEADDR);
    send_command(0);
    send_command(7);

    send_command(SSD1306_COLUMNADDR);
    send_command(0);
    send_command(127);

    _i2c->lock();
    _i2c->start();
    _i2c->write(_slave_addr);
    _i2c->write(SSD1306_DATA_MODE);
    for (int i = 0; i < len; i++) {
        _i2c->write(buf[i]);
    }
    _i2c->stop();
    _i2c->unlock();
}

//
// Lifted from AdaFruit library
//
void SSD1306::init_device() {
    send_command(SSD1306_DISPLAYOFF);                    // 0xAE
    send_command(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
    send_command(0x80);                                  // the suggested ratio 0x80

    send_command(SSD1306_SETMULTIPLEX);                  // 0xA8
    send_command(SSD1306_LCDHEIGHT - 1);

    send_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
    send_command(0x0);                                   // no offset
    send_command(SSD1306_SETSTARTLINE);            // line #0
    send_command(SSD1306_CHARGEPUMP);                    // 0x8D
    send_command(0x14);

    send_command(SSD1306_MEMORYMODE);                    // 0x20
    send_command(0x00);                                  // 0x0 act like ks0108
    send_command(SSD1306_SEGREMAP | 0x1);
    send_command(SSD1306_COMSCANDEC);


    send_command(SSD1306_SETCOMPINS);                    // 0xDA
    send_command(0x12);
    send_command(SSD1306_SETCONTRAST);                   // 0x81
    send_command(0xCF);

    send_command(SSD1306_SETPRECHARGE);                  // 0xd9
    send_command(0xF1);
    send_command(SSD1306_SETVCOMDETECT);                 // 0xDB
    send_command(0x40);
    send_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
    send_command(SSD1306_NORMALDISPLAY);                 // 0xA6

    send_command(SSD1306_DEACTIVATE_SCROLL);

    send_command(SSD1306_DISPLAYON);
}