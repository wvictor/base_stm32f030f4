#ifndef _SSD1306_H_
#define _SSD1306_H_

#include "mbed.h"

#define SSD1306_ADDRESS 0x78
#define SSD1306_FRAMEBUF_SIZE 1024

#define SSD1306_COMMAND_MODE 0x00
#define SSD1306_DATA_MODE 0x40

#define SSD1306_LCDWIDTH                  128
#define SSD1306_LCDHEIGHT                 64

#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF

#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA

#define SSD1306_SETVCOMDETECT 0xDB

#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9

#define SSD1306_SETMULTIPLEX 0xA8

#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10

#define SSD1306_SETSTARTLINE 0x40

#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR   0x22

#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8

#define SSD1306_SEGREMAP 0xA0

#define SSD1306_CHARGEPUMP 0x8D

#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2

#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A



class SSD1306 {
public:
    SSD1306(I2C *i__i2c, int i__slave_addr);
    void init();
    void test_display();
    void clear_display();
    void clear_fb();
    void display();
    void set_fb_byte(int page, int col, char val);

    void set_font_data(const unsigned char *data, int byte_rows, int cols, const char *chars, int char_count);
    bool draw_char(int byte_row, int col, char c);

private:
    I2C *_i2c;
    int _slave_addr;
    char _framebuf[SSD1306_FRAMEBUF_SIZE];
    const unsigned char *_fontdata;
    int _font_byte_rows;
    int _font_columns;
    const char *_font_chars;
    int _font_char_count;

    void send_command(char cmd);
    void send_data(const char *buf, int len);
    void init_device();



};
#endif // _SSD1306_H_